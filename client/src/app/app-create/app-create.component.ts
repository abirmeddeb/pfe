import { Component,OnInit,Input } from '@angular/core';
import {Post} from '../post.model' ;

import { ActivatedRoute, ParamMap } from "@angular/router";
import {Type } from '../type';
import { NgForm, FormGroup, FormControl,Validators } from '@angular/forms';
import { PostsService } from '../posts.service';

  

@Component({
  selector: 'app-app-create',
  templateUrl: './app-create.component.html',
  styleUrls: ['./app-create.component.css']
})
export class AppCreateComponent implements OnInit{
  entredTitle="";
  entreddescription="";
  typSelected="";
  post: Post;
  isLoading=false;
  form :FormGroup;
  filePreview:string;
  private mode = "create";
  private postId: string;

  @Input() types:Type []=[];
  ngOnInit(){
    this.form = new FormGroup({
      title:new FormControl(null,{validators:[Validators.required]}),
      description:new FormControl(null,{validators:[Validators.required]} ),
      type:new FormControl(null ),
      file: new FormControl(null,{validators:[Validators.required]})
    });
  this.types=[
    {Id:1,Name:"angular"},
    {Id:2,Name:"NodeJs"},
    {Id:3,Name:"Python"},
    {Id:4,Name:"Java"},
    {Id:5,Name:"PHP"},
    {Id:6,Name:"Tomcat"} 
  ];
  this.route.paramMap.subscribe((paramMap: ParamMap) => {
    if (paramMap.has("postId")) {
      this.mode = "edit";
      this.postId = paramMap.get("postId");
      this.isLoading = true;
      this.postsService.getPost(this.postId).subscribe(postData => {
        this.isLoading = false;
        this.post = {id: postData._id, title: postData.title, description: postData.description,type: postData.type,filePath:null};
        this.form.setValue({'title': this.post.title,'description':this.post.description,'type':this.post.type
      });
      });
    } else {
      this.mode = "create";
      this.postId = null;
    }
  });  

}

 

 
constructor(
  public postsService: PostsService,
  public route: ActivatedRoute
) {}


onFilePicked(event: Event){
  const file = (event.target as HTMLInputElement).files[0];
  this.form.patchValue({ file :file});
  this.form.get("file").updateValueAndValidity();
  
  const reader =new FileReader();
  
  // read file as data url

  reader.onload = () => { // called once readAsDataURL is completed
    this.filePreview = reader.result as string;
};
reader.readAsDataURL(file);}


onSavePost() {
  if (this.form.invalid) {
    return;
  }
  this.isLoading = true;
  if (this.mode === "create") {	
this.postsService.addPost(this.form.value.title,this.form.value.description,this.form.value.type,this.form.value.file);

    } else {
    this.postsService.updatePost(
      this.postId,
      this.form.value.title,
      this.form.value.description,
      this.form.value.type);
  }
this.form.reset();
}
}


  
