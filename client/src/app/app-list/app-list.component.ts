import { Component,OnInit,OnDestroy } from '@angular/core';
import {Post} from '../post.model' ;
import {Subscription} from 'rxjs';
import {PostsService} from '../posts.service'
import {PageEvent} from "@angular/material"
@Component({
  selector: 'app-app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.css']
})
export class AppListComponent implements OnInit, OnDestroy  {
 
  
    posts: Post[] = [];
    isLoading = false;
    totalPosts=10;
    postsPerPage = 2;
    pageSizeoptions = [1,2,3,4]
    private postsSub: Subscription;
  
    constructor(public postsService: PostsService) {}
  
    ngOnInit() {
      this.isLoading = true;
      this.postsService.getPosts(this.postsPerPage,1);
      this.postsSub = this.postsService.getPostUpdateListener()
        .subscribe((posts: Post[]) => {
          this.isLoading = false;
          this.posts = posts;
        });
    }
    onChangedPage(pageData:PageEvent){
console.log(pageData);
    }
  
  
    onDelete(postId:string) {
      this.postsService.deletePost(postId);
    }
  
    ngOnDestroy() {
      this.postsSub.unsubscribe();
    }
  }
  
  
  
  