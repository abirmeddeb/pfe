import { BrowserModule } from '@angular/platform-browser';
import {MatPaginatorModule} from '@angular/material/paginator'; 
import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import {ReactiveFormsModule} from '@angular/forms' 
import { RouterModule, Routes } from '@angular/router'
import { AppComponent } from './app.component'
import { ProfileComponent } from './profile/profile.component'
import { RegisterComponent } from './register/register.component'
import { HomeComponent } from './home/home.component'
import { AuthenticationService } from './authentication.service'
import { AuthGuardService } from './auth-guard.service'
import { FalseComponent } from './false/false.component'
import { FaasComponent } from './faas/faas.component'
import {  PaasComponent } from './paas/paas.component';
import { ChoiceComponent } from './choice/choice.component';
import { LoginComponent } from './login/login.component';
import { TestcComponent } from './testc/testc.component';
import { StartsofpaasComponent } from './startsofpaas/startsofpaas.component';
import { Step2Component } from './step2/step2.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApplComponent } from './appl/appl.component';
import { AppCreateComponent } from './app-create/app-create.component';
import { AppListComponent } from './app-list/app-list.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; 
import { MatButtonModule, MatInputModule, MatSelectModule, MatIconModule, MatCardModule, MatToolbarModule, MatMenuModule } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion'; 
import { PostsService } from './posts.service';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'false', component: FalseComponent },
  { path: 'paas', component: PaasComponent,canActivate: [AuthGuardService] },
  { path: 'faas', component: FaasComponent ,canActivate: [AuthGuardService] },
  { path: 'test', component: TestcComponent },
  { path: 'choice', component: ChoiceComponent ,canActivate: [AuthGuardService] },
  { path: 'step2', component: Step2Component,canActivate: [AuthGuardService] },
  { path: 'dashboard', component: DashboardComponent ,canActivate: [AuthGuardService] },
  { path: 'startsofpaas', component: StartsofpaasComponent ,canActivate: [AuthGuardService]},
  
  { path: 'Appl', component: ApplComponent ,canActivate: [AuthGuardService] },



  {

    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService]
  }
]

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    FalseComponent,
    FaasComponent,
    PaasComponent,
    ChoiceComponent,
    TestcComponent,
    StartsofpaasComponent,
    Step2Component,
    DashboardComponent,
    ApplComponent,
    AppCreateComponent,
    AppListComponent,
    
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatPaginatorModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,    
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule, MatCardModule, MatToolbarModule, MatMenuModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule

   
  
  ],
  providers: [AuthenticationService, AuthGuardService,PostsService],
  bootstrap: [AppComponent]
})
export class AppModule {}