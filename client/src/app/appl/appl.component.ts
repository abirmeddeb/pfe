import { Component, OnInit } from '@angular/core';
import { AuthenticationService, UserDetails } from '../authentication.service'
@Component({
  selector: 'app-appl',
  templateUrl: './appl.component.html',
  styleUrls: ['./appl.component.css']
})
  export class ApplComponent {
    details: UserDetails
  
    constructor(private auth: AuthenticationService) {}
  
    ngOnInit() {
      this.auth.profile().subscribe(
        user => {
          this.details = user
        },
        err => {
          console.error(err)
        }
      )
    }
  }


  
