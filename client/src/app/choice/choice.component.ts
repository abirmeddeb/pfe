import { Component } from '@angular/core'
import { AuthenticationService, UserDetails } from '../authentication.service'

@Component({
  selector: 'app-root',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.css']})
export class ChoiceComponent {
  details: UserDetails

  constructor(private auth: AuthenticationService) {}

  ngOnInit() {
    this.auth.profile().subscribe(
      user => {
        this.details = user
      },
      err => {
        console.error(err)
      }
    )
  }
}