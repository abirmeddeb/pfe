import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaasComponent } from './faas.component';

describe('FaasComponent', () => {
  let component: FaasComponent;
  let fixture: ComponentFixture<FaasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
