import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaasComponent } from './paas.component';

describe('PaasComponent', () => {
  let component: PaasComponent;
  let fixture: ComponentFixture<PaasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
