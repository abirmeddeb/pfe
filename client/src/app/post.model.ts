export interface  Post {
    id:string;
    title: string ;
    description: string;
    type:string;
    filePath:string
}
