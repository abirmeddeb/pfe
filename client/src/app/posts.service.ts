import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

import { Post } from "./post.model";

@Injectable({ providedIn: "root" })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();

  constructor(private http: HttpClient, private router: Router) {}

  getPosts(postsPerPage:number,currentPage:number) {
    const queryParam = `?pagesize=${postsPerPage}$page=${currentPage}`;
    this.http
      .get<{ message: string; posts: any }>("http://localhost:3000/posts" + queryParam)
      .pipe(
        map(postData => {
          return postData.posts.map(post => {
            return {
              id: post._id,
              description: post.description,
              title: post.title,
             type:post.type,
             filePath:post.filePath
              
            };
          });
        })
      )
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getPost(id: string) {
    return this.http.get<{ _id: string; title:string; description:string,type:string }>(
      "http://localhost:3000/posts/" + id
    );
  }

  addPost(title: string, description: string,type:string,file:File) {
    const postData = new FormData();
    postData.append('title',title);
    postData.append('description',description);
    postData.append('type',type);
    postData.append('file',file,title)
    this.http
      .post<{ message: string; post: Post }>(
        "http://localhost:3000/posts",
        postData
      )
      .subscribe(responseData => {
        const post: Post = {id:responseData.post.id,title: title,description: description,type: type,filePath:responseData.post.filePath};
        const id = responseData.post.id;
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
        this.router.navigate(["/Appl"]);
      });
  }

  updatePost(id:string,title:string,description:string,type:string) {
    const post: Post = { id:id,title:title,description:description,type:type,filePath:null };
    this.http
      .put("http://localhost:3000/posts/" + id, post)
      .subscribe(response => {
        const updatedPosts = [...this.posts];
        const oldPostIndex = updatedPosts.findIndex(p => p.id === post.id);
        updatedPosts[oldPostIndex] = post;
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
        this.router.navigate(["/Appl"]);
      });
  }

  deletePost(postId: string) {
    this.http
      .delete("http://localhost:3000/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
}
