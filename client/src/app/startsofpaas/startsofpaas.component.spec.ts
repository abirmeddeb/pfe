import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartsofpaasComponent } from './startsofpaas.component';

describe('StartsofpaasComponent', () => {
  let component: StartsofpaasComponent;
  let fixture: ComponentFixture<StartsofpaasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartsofpaasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartsofpaasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
