const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create Schema
const postSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    require:true
  
  },
  type: {
    type: String,
    require:true
  },
  filePath: {
    type: String ,
    require:true
  }
  
})
module.exports  = mongoose.model('Post', postSchema)