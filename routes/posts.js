const express = require('express')
const router = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const Post = require('../models/post')
router.use(cors())

const multer = require("multer")
const storage = multer.diskStorage({
  destination: (req, file , cb ) => {
  cb(null,"file");
  },
  filename:(req,file,cb) => {
    const name = file.originalname.toLocaleLowerCase().split(' ').join('-');
    cb(null,name + '-'+ Date.now() + '.' );
  }
});


router.post("", multer({storage:storage}).single("file"), (req, res, next) => {
  const url =req.protocol + '://' + req.get("host");
    const post = new Post({
      title: req.body.title,
      description: req.body.description,
      type:req.body.type,
      filePath:url + "/file" +req.file.filename

    });
    post.save().then(createdPost => {
      res.status(201).json({
        message: "Post added successfully",
        post :{
          id: createdPost._id,
        title:createdPost.tite,
        title:createdPost.description,
        title:createdPost.type,
        filePath:createdPost.filePath

        }
       
      });
    });
   
    
  });
  
  router.put("/:id", (req, res, next) => {
    const post = new Post({
      _id: req.body.id,
      title: req.body.title,
      content: req.body.content
    });
    Post.updateOne({ _id: req.params.id }, post).then(result => {
      res.status(200).json({ message: "Update successful!" });
    });
    
  });
  

  router.get("", (req, res, next) => {
   const pageSize = +req.query.pagesize;
   const currentPage = +req.query.page;
   const postQuery = Post.find();
   if (pageSize && currentPage){
     postQuery
     .skip(pageSize * (currentPage - 1))
     .limit(pageSize);

   }
    postQuery.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });
  
  router.get("/:id", (req, res, next) => {
    Post.findById(req.params.id).then(post => {
      if (post) {
        res.status(200).json(post);
      } else {
        res.status(404).json({ message: "Post not found!" });
      }
    });
  });
 
  
  

  router.delete("/posts/:id", (req, res, next) => {
    Post.deleteOne({ _id: req.params.id }).then(result => {
      console.log(result);
      res.status(200).json({ message: "Post deleted!" });
    });
  });




module.exports = router