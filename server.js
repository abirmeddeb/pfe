var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var app = express()
const mongoose = require('mongoose')
var port = process.env.PORT || 3000
const path=require("path")
app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({ extended: false }))
  app.use("/file",express.static(path.join("file")));
const mongoURI = 'mongodb://localhost:27017/cloudDB'

mongoose
  .connect(
    mongoURI,
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err))
  ;

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});
var Users = require('./routes/Users')

app.use('/users', Users)

var postsRoutes  = require('./routes/posts')


app.use('/posts', postsRoutes)
app.listen(port, function() {
  console.log('Server is running on port: ' + port)
})
 